'use strict';

angular.module('myApp').directive('createSubGroup', function ($timeout, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            groupModel: '=',
        },
        templateUrl: 'views/create.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = SubgroupModel;

            ctrl.groupModel = $scope.groupModel;
            
            ctrl.errors = {};
            ctrl.tmpModel = new BaseModel();
            ctrl.formId = "createFormId-" + BaseModel.COLLECTION_NAME + '-t' + (new Date().getTime());

            ctrl.contentPath = 'modules/subgroups/_formContent.html';
            
            ctrl.openForm = function () {
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                document.getElementById(ctrl.formId).style.display = 'none';
            };

            ctrl.confirm = function () {

                filter();

                ctrl.tmpModel.group_path = ctrl.groupModel.getPath();

                if (!validate()) {
                    console.error(ctrl.errors);
                    return;
                }

                create().then(success => {
                    ctrl.tmpModel = new BaseModel();
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.name = ctrl.tmpModel.name.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.students_count = ctrl.tmpModel.students_count ? Number(ctrl.tmpModel.students_count) : null;
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.name) {
                    errors.name = "Ім'я є обов'язковим";
                }

                if (!ctrl.tmpModel.group_path) {
                    errors.group_path = "Група є обов'язковою";
                }

                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function create() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).add(fullData);
            }
        }
    };
});