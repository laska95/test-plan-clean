'use strict';

angular.module('myApp').directive('deleteSubGroup', function ($window) {
    return {
        restrict: 'E',
        scope: {
            'baseModel': '=',
            'redirectTo': '@',
        },
        templateUrl: 'views/delete.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {
            const ctrl = this;
            const BaseModel = SubgroupModel;

            ctrl.errors = {};
            ctrl.tmpModel = $scope.baseModel;
            ctrl.formId = "deleteFormId-" + ctrl.tmpModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/subgroups/_deleteContent.html';

            ctrl.canDelete = false;
            ctrl.usedInList = [];

            ctrl.openForm = function () {
                validate();
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                deleteModel().then(success => {
                    if ($scope.redirectTo){
                        $window.location.href = $scope.redirectTo;
                    }
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            async function deleteModel(){
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).delete();
            }

            function validate() {
                let thisPath = ctrl.tmpModel.getPath();
                ctrl.usedInList = [];

                //у заняття інша структура, тому на них пишемо перевірку вручну
                if (LessonPlanModel.STORAGE) {
                    LessonPlanModel.STORAGE.forEach(item => {
                        let index = item.group_pathes.findIndex( elem => {
                            return elem === thisPath;
                        });
                        if (index >= 0) {
                            ctrl.usedInList.push({
                                'model_name': item.getPrettyModelName(),
                                'item_name': item.getPrettyItemName(),
                                'url': item.getViewUrl()
                            });
                        }
                    });
                }

                ctrl.canDelete = (ctrl.usedInList.length === 0);
            }
        }
    };
});