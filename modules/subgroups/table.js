'use strict';

angular.module('myApp').directive('subGroupTable', function ($timeout, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            groupModel: '=',
        },
        templateUrl: 'modules/subgroups/table.html',
        controllerAs: 'tblCtrl',
        controller: function ($scope, $element, $attrs) {
            const ctrl = this;

            ctrl.groupModel = $scope.groupModel;
            ctrl.modelList = [];

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerSubgroup;
            }, function () {
                ctrl.modelList = filterByAttr(SubgroupModel, 'group_path', ctrl.groupModel.getPath());
            });
        }
    };
});
