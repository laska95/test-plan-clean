'use strict';

function GroupsCtrl($scope, $timeout, $rootScope) {

    const ctrl = this;

    ctrl.modelList = [];

    $scope.$watch(function () {
       return $rootScope.globalChangeTriggerGroup;
    }, function () {
        ctrl.modelList = GroupModel.STORAGE;
    });
}

function GroupsViewCtrl($scope, $timeout, $routeParams, $location, $rootScope) {

    const ctrl = this;
    const modelId = $routeParams.groupId;

    ctrl.model = null;

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerGroup;
    }, function () {
        ctrl.model = findModelById(GroupModel, modelId);
    });


    const firstWeek = 1;

    ctrl.modelListFirst = [];
    ctrl.modelListSecond = [];
    ctrl.groupAndSubGroupFullNames = {};

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLessonPlan;
    }, function () {

        if (!ctrl.model){
            return;
        }

        let first = [];
        let second = [];

        let subGroups = filterByAttr(SubgroupModel, 'group_path', ctrl.model.getPath());

        LessonPlanModel.STORAGE.forEach((lesson) => {

            if (!lesson.group_pathes){
                return;
            }

            let index = lesson.group_pathes.findIndex(path => {
                let isThisGroup = (path == ctrl.model.getPath());
                let isThisSubGroup = subGroups.findIndex((subGroup) => {
                   return (path == subGroup.getPath());
                }) >= 0;
                return isThisGroup || isThisSubGroup;
            });

            if (index < 0){
                return; //continue
            }

            if (lesson.weekparity == firstWeek) {
                first.push(lesson);
            } else {
                second.push(lesson);
            }
        });

        ctrl.modelListFirst = first;
        ctrl.modelListSecond = second;
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerGroup + "." + $rootScope.globalChangeTriggerSubgroup;
    }, function () {
        updateGroupFullName();
    });

    function updateGroupFullName(){

        let list = {};

        GroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = model.name;
        });

        SubgroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = `(${list[model.group_path]}) ${model.name}`;
        });

        ctrl.groupAndSubGroupFullNames = list;
    }

}

angular.module('myApp.groups', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/groups', {
            templateUrl: 'modules/groups/index.html',
        });
        $routeProvider.when('/groups/:groupId', {
            templateUrl: 'modules/groups/view.html',
        });
    }])
    .controller('GroupsCtrl', GroupsCtrl)
    .controller('GroupsViewCtrl', GroupsViewCtrl);
