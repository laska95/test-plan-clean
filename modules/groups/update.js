'use strict';

angular.module('myApp').directive('updateGroup', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
           'baseModel': '='
        },
        templateUrl: 'views/update.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = GroupModel;

            ctrl.errors = {};
            ctrl.tmpModel = null;
            ctrl.formId = "updateFormId-" + $scope.baseModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/groups/_formContent.html';

            ctrl.cathedralList = CathedralModel.STORAGE;

            ctrl.openForm = function () {
                ctrl.tmpModel = constructBySnapshot(BaseModel, $scope.baseModel._snapshot);
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                filter();

                if (!validate()){
                    console.error(ctrl.errors);
                    return;
                }

                update().then(success => {
                    ctrl.tmpModel = null;
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.name = ctrl.tmpModel.name.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.students_count = ctrl.tmpModel.students_count ? Number(ctrl.tmpModel.students_count) : null;
                ctrl.tmpModel.cathedral_path = ctrl.tmpModel.cathedral_path ? ctrl.tmpModel.cathedral_path : null;
                ctrl.tmpModel.comment = ctrl.tmpModel.comment.replace(/(^\s*)|(\s*$)/, '');
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.name) {
                    errors.name = "Ім'я є обов'язковим";
                }

                if (!ctrl.tmpModel.cathedral_path) {
                    errors.cathedral_path = "Кафедра є обов'язковою";
                }

                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function update() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).update(fullData);
            }

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerCathedral;
            }, function () {
                ctrl.cathedralList = CathedralModel.STORAGE;
            });
        }
    };
});