'use strict';

angular.module('myApp').directive('createGroup', function ($timeout, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'views/create.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = GroupModel;

            ctrl.errors = {};
            ctrl.tmpModel = new BaseModel();
            ctrl.formId = "createFormId-" + BaseModel.COLLECTION_NAME + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/groups/_formContent.html';

            ctrl.cathedralList = CathedralModel.STORAGE;

            ctrl.openForm = function () {
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                document.getElementById(ctrl.formId).style.display = 'none';
            };

            ctrl.confirm = function () {

                filter();

                if (!validate()) {
                    console.error(ctrl.errors);
                    return;
                }

                create().then(success => {
                    ctrl.tmpModel = new BaseModel();
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.name = ctrl.tmpModel.name.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.students_count = ctrl.tmpModel.students_count ? Number(ctrl.tmpModel.students_count) : null;
                ctrl.tmpModel.cathedral_path = ctrl.tmpModel.cathedral_path ? ctrl.tmpModel.cathedral_path : null;
                ctrl.tmpModel.comment = ctrl.tmpModel.comment.replace(/(^\s*)|(\s*$)/, '');
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.name) {
                    errors.name = "Ім'я є обов'язковим";
                }

                if (!ctrl.tmpModel.cathedral_path) {
                    errors.cathedral_path = "Кафедра є обов'язковою";
                }

                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function create() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).add(fullData);
            }

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerCathedral;
            }, function () {
                ctrl.cathedralList = CathedralModel.STORAGE;
            });
        }
    };
});