'use strict';

//Це оголошення директиви
angular.module('myApp').directive('createAuditory', function ($timeout, $rootScope) {
   return {
       restrict: 'E', //Беква E (element) означає що директива виклмкатиметься як кастомний тег html
       templateUrl: 'views/create.html', //частина сторінки яка відбудовуватиметься при викику директиви
       controllerAs: 'drCtrl',
       controller: function ($scope, $element, $attrs) {

           const ctrl = this;
           const BaseModel = AuditoryModel;

           ctrl.errors = {};
           ctrl.tmpModel = new BaseModel();
           ctrl.formId = "createFormId-" + BaseModel.COLLECTION_NAME + '-t' + (new Date().getTime());
           ctrl.contentPath = 'modules/auditories/_formContent.html';

           ctrl.cathedralList = CathedralModel.STORAGE;

           ctrl.openForm = function () {
               document.getElementById(ctrl.formId).style.display = 'block';
           };

           ctrl.closeForm = function () {
               document.getElementById(ctrl.formId).style.display = 'none';
           };

           ctrl.confirm = function () {

               filter();

               if (!validate()){
                   console.error(ctrl.errors);
                   return;
               }

               create().then(success => {
                   ctrl.tmpModel = new BaseModel();
                   ctrl.closeForm();
               }, error => {
                   console.error(error);
                   ctrl.errors = {"id": "Data base error!"};
               });
           };

           function filter() {
               ctrl.tmpModel.fullname = ctrl.tmpModel.fullname.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.size = ctrl.tmpModel.size.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.cathedral_path = ctrl.tmpModel.cathedral_path ? ctrl.tmpModel.cathedral_path : null;
           }

           function validate() {
               let errors = {};

               if (!ctrl.tmpModel.fullname) {
                   errors.fullname = "Повна назва є обов'язковою";
               }

               ctrl.errors = errors;
               return !Object.keys(errors).length;
           }

           async function create() {
               let fullData = ctrl.tmpModel.getFullData();
               return await DB.collection(BaseModel.COLLECTION_NAME).add(fullData);
           }

           $scope.$watch(function () {
               return $rootScope.globalChangeTriggerCathedral;
           }, function () {
               ctrl.cathedralList = CathedralModel.STORAGE;
           });
       }
   };
});