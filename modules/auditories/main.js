'use strict';

//Опис контроллера для виводу списку всіх аудиторій
function AuditoriesCtrl($scope, $timeout, $rootScope) {

    //Ангуляр має повно власнийх фіч, таких як $scope, $timeout, $rootScope тощо.
    //Щоб ними користуватися, їх треба передати в контроллер, що і зроблено вище.
    //Порядок не важливий.
    //$scope - область видимості поточного контроллера
    //$rootScope - глобальна область видимості

    //this - це системна змінна, кожна функція (крім стрілочних) має власний this.
    // Щоб не загубити this цієї функції-контроллера, зберігаємо його в окрему змінну
    const ctrl = this;

    //список аудиторій що виводитиметося на сторінку.
    //все що ми оголосили як ctrl.*** можна буде витягнути на html сторінці як
    // <Скорочена назва контроллера>.<Назва змінної>
    ctrl.modelList = [];

    //Функція scope.$watch дозволяє слідкувати за змінами даних і правильно на них реагувати
    //хоча вона їсть багато ресурсів(
    $scope.$watch(function () {
        //якщо час, коли були останній раз загружені дані по аудиторіїях, змінився
        //ми оновлюємо список аудиторій які треба виводити
        return $rootScope.globalChangeTriggerAuditory;
    }, function () {
        ctrl.modelList = AuditoryModel.STORAGE;
    });

}

//опис контроллера для перегляду однієї аудиторії
function AuditoriesViewCtrl($scope, $timeout, $routeParams, $location, $rootScope){

    const ctrl = this;
    const modelId = $routeParams.auditoryId;

    ctrl.model = null;

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerAuditory;
    }, function () {
        ctrl.model = findModelById(AuditoryModel, modelId);
    });
}

//Ось тут ми оголошуєио новий модуль по "Ангулярівськи"
angular.module('myApp.auditories', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        //Описуємо роботу з url для цього модуля.
        //якщо в url вказано /auditories, то відкрисаємо і відбудовуємо файл modules/auditories/index.html
        $routeProvider.when('/auditories', {
            templateUrl: 'modules/auditories/index.html',
        });
        $routeProvider.when('/auditories/:auditoryId', {
            templateUrl: 'modules/auditories/view.html',
        });
    }])
    //Перетворюємо вище описані фугкції в справжні контроллери
    //Перший аргумент - це повна назва контроллеру в html файлах
    //Другий - назва функції що описує роботу контроллера
    //для зручності вони одинакові
    .controller('AuditoriesCtrl', AuditoriesCtrl)
    .controller('AuditoriesViewCtrl', AuditoriesViewCtrl);
