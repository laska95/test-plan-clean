'use strict';

angular.module('myApp').directive('updateLesson', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
           'baseModel': '='
        },
        templateUrl: 'views/update.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = LessonPlanModel;

            ctrl.errors = {};
            ctrl.tmpModel = null;
            ctrl.formId = "updateFormId-" + $scope.baseModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/lessons/_formContent.html';

            ctrl.weekparityList = LessonPlanModel.weekparityList;
            ctrl.weekdayList = LessonPlanModel.weekdayList;
            ctrl.numberList = LessonPlanModel.numberList;
            ctrl.lessonTypeList = LessonPlanModel.lessonTypeList;

            ctrl.auditoryList = AuditoryModel.STORAGE;
            ctrl.lecturerList = LecturerModel.STORAGE;
            ctrl.groupList = GroupModel.STORAGE;
            ctrl.subjectList = SubjectModel.STORAGE;

            ctrl.openForm = function () {
                ctrl.tmpModel = constructBySnapshot(BaseModel, $scope.baseModel._snapshot);
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                filter();

                if (!validate()){
                    console.error(ctrl.errors);
                    return;
                }

                update().then(success => {
                    ctrl.tmpModel = null;
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.weekparity = Number(ctrl.tmpModel.weekparity);
                ctrl.tmpModel.weekday = Number(ctrl.tmpModel.weekday);
                ctrl.tmpModel.number = Number(ctrl.tmpModel.number);
                ctrl.tmpModel.lessontype = Number(ctrl.tmpModel.lessontype);
                ctrl.tmpModel.comment = ctrl.tmpModel.comment ? ctrl.tmpModel.comment + "" : "";

                ctrl.tmpModel.subject_path = ctrl.tmpModel.subject_path ? ctrl.tmpModel.subject_path : null;
                ctrl.tmpModel.auditory_path = ctrl.tmpModel.auditory_path ? ctrl.tmpModel.auditory_path : null;
                ctrl.tmpModel.lecturer_path = ctrl.tmpModel.lecturer_path ? ctrl.tmpModel.lecturer_path : null;
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.weekparity) {
                    errors.weekparity = "Обов'язкове поле!";
                }
                if (!ctrl.tmpModel.weekday) {
                    errors.weekday = "Обов'язкове поле!";
                }
                if (!ctrl.tmpModel.number) {
                    errors.number = "Обов'язкове поле!";
                }

                if (!ctrl.tmpModel.lessontype) {
                    errors.lessontype = "Обов'язкове поле!";
                }

                if (!ctrl.tmpModel.subject_path) {
                    errors.subject_path = "Обов'язкове поле!";
                }
                if (!ctrl.tmpModel.auditory_path) {
                    errors.auditory_path = "Обов'язкове поле!";
                }

                if (!ctrl.tmpModel.lecturer_path) {
                    errors.lecturer_path = "Обов'язкове поле!";
                }

                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function update() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).update(fullData);
            }

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerAuditory;
            }, function () {
                ctrl.auditoryList = AuditoryModel.STORAGE;
            });

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerGroup;
            }, function () {
                ctrl.groupList = GroupModel.STORAGE;
            });

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerLecturer;
            }, function () {
                ctrl.lecturerList = LecturerModel.STORAGE;
            });

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerSubject;
            }, function () {
                ctrl.subjectList = SubjectModel.STORAGE;
            });
        }
    };
});