'use strict';

angular.module('myApp').directive('groupSelector', function ($timeout, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            'ngModel': '=',
        },
        templateUrl: 'modules/lessons/groupSelector.html',
        controllerAs: 'gSelectorCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;

            ctrl.groupList = GroupModel.STORAGE;

            ctrl.selectedItems = {};
            ctrl.showSubgroups = {};

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerGroup + "." + $rootScope.globalChangeTriggerSubgroup;
            }, function () {
                ctrl.groupList = GroupModel.STORAGE;

                angular.forEach(ctrl.groupList, function (item) {
                    let subgroupsCount = item.getSubGroups().length;
                    if (subgroupsCount === 0) {
                        ctrl.showSubgroups[item.id] = 1;
                    } else if (!ctrl.showSubgroups[item.id] || ctrl.showSubgroups[item.id] == 1) {
                        ctrl.showSubgroups[item.id] = 2;
                    }
                });
            });

            ctrl.showHideSubgroups = function (groupId) {
                if (ctrl.showSubgroups[groupId] == 2) {
                    ctrl.showSubgroups[groupId] = 3;
                } else if (ctrl.showSubgroups[groupId] == 3) {
                    ctrl.showSubgroups[groupId] = 2;
                }
            };

            $scope.$watch('ngModel', function () {

                if (!$scope.ngModel || $scope.ngModel.length == 0) {
                    ctrl.selectedItems = {};
                    return;
                }

                $scope.ngModel.forEach((key) => {
                    ctrl.selectedItems[key] = true;
                });
            });

            $scope.$watch(function () {
                return JSON.stringify(ctrl.selectedItems);
            }, function () {
                let newValue = [];
                Object.keys(ctrl.selectedItems).forEach((key) => {
                    if (ctrl.selectedItems[key]) {
                        newValue.push(key);
                    }
                });
                $scope.ngModel = newValue;
            });

        }
    };
});