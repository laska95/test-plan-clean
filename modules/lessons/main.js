'use strict';

function LessonsCtrl($scope, $timeout, $rootScope) {

    const ctrl = this;

    const firstWeek = 1;

    ctrl.modelListFirst = [];
    ctrl.modelListSecond = [];

    ctrl.groupAndSubGroupFullNames = {};

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLessonPlan;
    }, function () {

        let first = [];
        let second = [];

        LessonPlanModel.STORAGE.forEach((lesson) => {
            if (lesson.weekparity == firstWeek) {
                first.push(lesson);
            } else {
                second.push(lesson);
            }
        });

        ctrl.modelListFirst = first;
        ctrl.modelListSecond = second;
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerGroup + "." + $rootScope.globalChangeTriggerSubgroup;
    }, function () {
        updateGroupFullName();
    });

    function updateGroupFullName(){

        let list = {};

        GroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = model.name;
        });

        SubgroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = `(${list[model.group_path]}) ${model.name}`;
        });

        ctrl.groupAndSubGroupFullNames = list;
    }

}

function LessonsViewCtrl($scope, $timeout, $routeParams, $location, $rootScope) {

    const ctrl = this;
    const modelId = $routeParams.lessonId;

    ctrl.model = null;
    ctrl.groupAndSubgroupList = [];

    ctrl.selectedGroups = [];

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLessonPlan;
    }, function () {
        ctrl.model = findModelById(LessonPlanModel, modelId);
        updateGroupList();
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerGroup + "." + $rootScope.globalChangeTriggerSubgroup;
    }, function () {
        updateGroupList();
    });

    function updateGroupList() {

        if (!ctrl.model || !ctrl.model.group_pathes) {
            ctrl.groupAndSubgroupList = [];
            return;
        }

        let newList = [];
        (ctrl.model.group_pathes || []).forEach((path) => {

            if (/^Group/.test(path)) {
                let model = findModelByPath(GroupModel, path);

                if (!model){
                    return; //continue
                }

                let obj = {
                    'is_group': true,
                    'model': model,
                    'group_id': model.id,
                    'full_name': model.name
                };
                newList.push(obj);
            } else {
                let model = findModelByPath(SubgroupModel, path);

                if (!model){
                    return; //continue
                }

                let group = model.getGroupModel();

                let obj = {
                    'is_group': false,
                    'model': model,
                    'group_id': group.id,
                    'full_name': `(${group.name}) ${model.name}`
                };
                newList.push(obj);
            }

        });
        ctrl.groupAndSubgroupList = newList;
    }

}

angular.module('myApp.lessons', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/lessons', {
            templateUrl: 'modules/lessons/index.html',
        });
        $routeProvider.when('/lessons/:lessonId', {
            templateUrl: 'modules/lessons/view.html',
        });
    }])
    .controller('LessonsCtrl', LessonsCtrl)
    .controller('LessonsViewCtrl', LessonsViewCtrl);
