'use strict';

angular.module('myApp').directive('deleteLesson', function ($window) {
    return {
        restrict: 'E',
        scope: {
            'baseModel': '=',
            'redirectTo': '@',
        },
        templateUrl: 'views/delete.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {
            const ctrl = this;
            const BaseModel = LessonPlanModel;

            ctrl.errors = {};
            ctrl.tmpModel = $scope.baseModel;
            ctrl.formId = "deleteFormId-" + ctrl.tmpModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/lessons/_deleteContent.html';

            ctrl.canDelete = true; //заняття завжди можна видалити

            ctrl.openForm = function () {
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                deleteModel().then(success => {
                    if ($scope.redirectTo){
                        $window.location.href = $scope.redirectTo;
                    }
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            async function deleteModel(){
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).delete();
            }
        }
    };
});