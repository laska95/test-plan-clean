'use strict';

function SubjectsCtrl($scope, $timeout, $rootScope) {

    const ctrl = this;

    ctrl.modelList = [];
    ctrl.cathedralList = [];

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerSubject;
    }, function () {
        ctrl.modelList = SubjectModel.STORAGE;
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerCathedral;
    }, function () {
        ctrl.cathedralList = CathedralModel.STORAGE;
    });

}

function SubjectsViewCtrl($scope, $timeout, $routeParams, $location, $rootScope) {

    const ctrl = this;
    const modelId = $routeParams.subjectId;

    ctrl.model = null;
    ctrl.cathedralList = [];

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerSubject;
    }, function () {
        ctrl.model = findModelById(SubjectModel, modelId);
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerCathedral;
    }, function () {
        ctrl.cathedralList = CathedralModel.STORAGE;
    });
}

angular.module('myApp.subjects', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/subjects', {
            templateUrl: 'modules/subjects/index.html',
        });
        $routeProvider.when('/subjects/:subjectId', {
            templateUrl: 'modules/subjects/view.html',
        });
    }])
    .controller('SubjectsCtrl', SubjectsCtrl)
    .controller('SubjectsViewCtrl', SubjectsViewCtrl);
