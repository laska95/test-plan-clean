'use strict';

angular.module('myApp').directive('updateSubject', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
           'baseModel': '='
        },
        templateUrl: 'views/update.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = SubjectModel;

            ctrl.errors = {};
            ctrl.tmpModel = null;
            ctrl.formId = "updateFormId-" + $scope.baseModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/subjects/_formContent.html';

            ctrl.cathedralList = CathedralModel.STORAGE;

            ctrl.openForm = function () {
                ctrl.tmpModel = constructBySnapshot(BaseModel, $scope.baseModel._snapshot);
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                filter();

                if (!validate()){
                    console.error(ctrl.errors);
                    return;
                }

                update().then(success => {
                    ctrl.tmpModel = null;
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.fullname = ctrl.tmpModel.fullname.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.shortname = ctrl.tmpModel.shortname.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.cathedral_path =  ctrl.tmpModel.cathedral_path ?  ctrl.tmpModel.cathedral_path : null;
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.shortname) {
                    errors.shortname = "Скорочена назва є обов'язковою";
                }

                if (!ctrl.tmpModel.fullname) {
                    errors.fullname = "Повна назва є обов'язковою";
                }


                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function update() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).update(fullData);
            }

            $scope.$watch(function () {
                return $rootScope.globalChangeTriggerCathedral;
            }, function () {
                ctrl.cathedralList = CathedralModel.STORAGE;
            });
        }
    };
});