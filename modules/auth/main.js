'use strict';

function AuthCtrl($scope, $timeout, $rootScope, $window) {

	const ctrl = this;

	ctrl.USER = null;

	ctrl.login = "";
	ctrl.password = "";

	ctrl.errorMsg = "";

	$scope.adminUsers = JSON.parse(JSON.stringify(ADMIN_USERS));

	ctrl.submitLogin = function() {

		let index = $scope.adminUsers.findIndex((item) => {
			return  ctrl.login == item;
		});
		if (index < 0){
			ctrl.errorMsg = "Помилка авторизації";
			ctrl.password = "";
			$timeout(() => $scope.$apply());
			return;
		}

		AUTH.signInWithEmailAndPassword(ctrl.login, ctrl.password).then((cred) => {
			console.log(cred.user);
			ctrl.password = "";
			$timeout(() => $scope.$apply());
		}, (error) => {
			ctrl.errorMsg = "Помилка авторизації";
			ctrl.password = "";
			$timeout(() => $scope.$apply());
		});
	};

	ctrl.logout = function(){
		AUTH.signOut();
		$timeout(() => $scope.$apply());
	};

	AUTH.onAuthStateChanged(user => {
		ctrl.USER = user;
		if (user){
			ctrl.login = user.login;
		}
		$timeout(() => $scope.$apply());
	});

}

angular.module('myApp.auth', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/auth', {
			templateUrl: 'modules/auth/index.html',
		});
	}])
	.controller('AuthCtrl', AuthCtrl);