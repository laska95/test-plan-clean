'use strict';

angular.module('myApp').directive('updateCathedral', function () {
    return {
        restrict: 'E',
        scope: {
           'baseModel': '='
        },
        templateUrl: 'views/update.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {

            const ctrl = this;
            const BaseModel = CathedralModel;

            ctrl.errors = {};
            ctrl.tmpModel = null;
            ctrl.formId = "updateFormId-" + $scope.baseModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/cathedrals/_formContent.html';

            ctrl.openForm = function () {
                ctrl.tmpModel = constructBySnapshot(BaseModel, $scope.baseModel._snapshot);
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                filter();

                if (!validate()){
                    console.error(ctrl.errors);
                    return;
                }

                update().then(success => {
                    ctrl.tmpModel = null;
                    ctrl.closeForm();
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            function filter() {
                ctrl.tmpModel.full_name = ctrl.tmpModel.full_name.replace(/(^\s*)|(\s*$)/, '');
                ctrl.tmpModel.short_name = ctrl.tmpModel.short_name.replace(/(^\s*)|(\s*$)/, '');
            }

            function validate() {
                let errors = {};

                if (!ctrl.tmpModel.full_name) {
                    errors.full_name = "Повна назва є обов'язковою";
                }

                if (!ctrl.tmpModel.short_name) {
                    errors.short_name = "Абривіатура є обов'язковою";
                }

                ctrl.errors = errors;
                return !Object.keys(errors).length;
            }

            async function update() {
                let fullData = ctrl.tmpModel.getFullData();
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).update(fullData);
            }
        }
    };
});