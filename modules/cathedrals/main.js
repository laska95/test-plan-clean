'use strict';

function CathedralsCtrl($scope, $timeout, $rootScope) {

    const ctrl = this;

    //список кафедр для таблиці
    ctrl.modelList = [];
    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerCathedral;
    }, function () {
        ctrl.modelList = CathedralModel.STORAGE;
    });
}

//перегляд даних про кафедру
function CathedralsViewCtrl($scope, $timeout, $routeParams, $location, $rootScope) {
    const ctrl = this;

    const modelId = $routeParams.cathedralId;

    //дані про поточну кафедру
    ctrl.model = null;
    ctrl.lecturerList = [];

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerCathedral;
    }, function () {
        ctrl.model = findModelById(CathedralModel, modelId);
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLecturer;
    }, function () {
        if (ctrl.model){
            ctrl.lecturerList = filterByAttr(LecturerModel, 'cathedral_path', ctrl.model.getPath());
        }
    });

}

angular.module('myApp.cathedrals', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/cathedrals', {
            templateUrl: 'modules/cathedrals/index.html',
        });
        $routeProvider.when('/cathedrals/:cathedralId', {
            templateUrl: 'modules/cathedrals/view.html',
        });
    }])
    .controller('CathedralsCtrl', CathedralsCtrl)
    .controller('CathedralsViewCtrl', CathedralsViewCtrl);