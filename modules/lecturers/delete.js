'use strict';

angular.module('myApp').directive('deleteLecturer', function ($window) {
    return {
        restrict: 'E',
        scope: {
            'baseModel': '=',
            'redirectTo': '@',
        },
        templateUrl: 'views/delete.html',
        controllerAs: 'drCtrl',
        controller: function ($scope, $element, $attrs) {
            const ctrl = this;
            const BaseModel = LecturerModel;

            ctrl.errors = {};
            ctrl.tmpModel = $scope.baseModel;
            ctrl.formId = "deleteFormId-" + ctrl.tmpModel.getPath() + '-t' + (new Date().getTime());
            ctrl.contentPath = 'modules/lecturers/_deleteContent.html';

            ctrl.canDelete = false;
            ctrl.usedInList = [];

            ctrl.openForm = function () {
                validate();
                document.getElementById(ctrl.formId).style.display = 'block';
            };

            ctrl.closeForm = function () {
                let element = document.getElementById(ctrl.formId);
                if (element){
                    element.style.display = 'none';
                }
            };

            ctrl.confirm = function () {

                deleteModel().then(success => {
                    if ($scope.redirectTo){
                        $window.location.href = $scope.redirectTo;
                    }
                }, error => {
                    console.error(error);
                    ctrl.errors = {"id": "Data base error!"};
                });
            };

            async function deleteModel(){
                return await DB.collection(BaseModel.COLLECTION_NAME).doc(ctrl.tmpModel.id).delete();
            }

            function validate() {
                let thisPath = ctrl.tmpModel.getPath();
                let canBeUsedIn = [LessonPlanModel];
                ctrl.usedInList = checkUsed(canBeUsedIn, 'lecturer_path', thisPath);
                ctrl.canDelete = (ctrl.usedInList.length === 0);
            }
        }
    };
});