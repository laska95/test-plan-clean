'use strict';

function LecturersCtrl($scope, $timeout, $rootScope) {

    const ctrl = this;

    ctrl.modelList = LecturerModel.STORAGE;

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLecturer;
    }, function () {
        ctrl.modelList = LecturerModel.STORAGE;
    });

}

function LecturersViewCtrl($scope, $timeout, $routeParams, $location, $rootScope) {
    const ctrl = this;

    const modelId = $routeParams.lecturerId;

    ctrl.model = null;

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLecturer;
    }, function () {
        ctrl.model = findModelById(LecturerModel, modelId);
    });

    const firstWeek = 1;

    ctrl.modelListFirst = [];
    ctrl.modelListSecond = [];

    ctrl.groupAndSubGroupFullNames = {};

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerLessonPlan;
    }, function () {

        let first = [];
        let second = [];

        LessonPlanModel.STORAGE.forEach((lesson) => {

            if (lesson.lecturer_path !== ctrl.model.getPath()){
                return; //continue
            }

            if (lesson.weekparity == firstWeek) {
                first.push(lesson);
            } else {
                second.push(lesson);
            }
        });

        ctrl.modelListFirst = first;
        ctrl.modelListSecond = second;
    });

    $scope.$watch(function () {
        return $rootScope.globalChangeTriggerGroup + "." + $rootScope.globalChangeTriggerSubgroup;
    }, function () {
        updateGroupFullName();
    });

    function updateGroupFullName(){

        let list = {};

        GroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = model.name;
        });

        SubgroupModel.STORAGE.forEach(model => {
            list[model.getPath()] = `(${list[model.group_path]}) ${model.name}`;
        });

        ctrl.groupAndSubGroupFullNames = list;
    }

}

angular.module('myApp.lecturers', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/lecturers', {
            templateUrl: 'modules/lecturers/index.html',
        });
        $routeProvider.when('/lecturers/:lecturerId', {
            templateUrl: 'modules/lecturers/view.html',
        });
    }])
    .controller('LecturersCtrl', LecturersCtrl)
    .controller('LecturersViewCtrl', LecturersViewCtrl);