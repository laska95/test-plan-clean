'use strict';

angular.module('myApp').directive('createLecturer', function ($timeout, $rootScope) {
   return {
       restrict: 'E',
       templateUrl: 'views/create.html',
       controllerAs: 'drCtrl',
       controller: function ($scope, $element, $attrs) {

           const ctrl = this;
           const BaseModel = LecturerModel;

           ctrl.errors = {};
           ctrl.tmpModel = new BaseModel();
           ctrl.formId = "createFormId-" + BaseModel.COLLECTION_NAME + '-t' + (new Date().getTime());
           ctrl.contentPath = 'modules/lecturers/_formContent.html';

           ctrl.cathedralList = CathedralModel.STORAGE;

           ctrl.openForm = function () {
               document.getElementById(ctrl.formId).style.display = 'block';
           };

           ctrl.closeForm = function () {
               document.getElementById(ctrl.formId).style.display = 'none';
           };

           ctrl.confirm = function () {

               filter();

               if (!validate()){
                   console.error(ctrl.errors);
                   return;
               }

               create().then(success => {
                   ctrl.tmpModel = new BaseModel();
                   ctrl.closeForm();
               }, error => {
                   console.error(error);
                   ctrl.errors = {"id": "Data base error!"};
               });
           };

           function filter() {
               ctrl.tmpModel.surname = ctrl.tmpModel.surname.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.name = ctrl.tmpModel.name.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.middlename = ctrl.tmpModel.middlename.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.degree = ctrl.tmpModel.degree.replace(/(^\s*)|(\s*$)/, '');
               ctrl.tmpModel.cathedral_path =  ctrl.tmpModel.cathedral_path ?  ctrl.tmpModel.cathedral_path : null;
           }

           function validate() {
               let errors = {};

               if (!ctrl.tmpModel.name) {
                   errors.name = "Ім'я є обов'язковим";
               }

               if (!ctrl.tmpModel.surname) {
                   errors.surname = "Прізвище є обов'язковим";
               }

               ctrl.errors = errors;
               return !Object.keys(errors).length;
           }

           async function create() {
               let fullData = ctrl.tmpModel.getFullData();
               return await DB.collection(BaseModel.COLLECTION_NAME).add(fullData);
           }

           $scope.$watch(function () {
               return $rootScope.globalChangeTriggerCathedral;
           }, function () {
               ctrl.cathedralList = CathedralModel.STORAGE;
           });
       }
   };
});