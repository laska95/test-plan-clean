'use strict';

//Опис структури даних та роботи з ними
//В js є також і класи (class) але мені не подобається працювати з ними. Там не можна явно оголошувати атрибути
function SubjectModel() {

    const Model = this;

    Model._snapshot = null;

    Model.id = null;
    Model.fullname = "";
    Model.shortname = "";
    Model.cathedral_path = null;


    Model.restoreBySnapshot = function(){
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.fullname = data.fullname;

        Model.shortname = data.shortname;
        Model.cathedral_path = data.cathedral_path;
    };

    Model.getPath = function () {
        return `${SubjectModel.COLLECTION_NAME}/${Model.id}`;
    };

    Model.getFullData = function () {
        return  {
            "fullname": Model.fullname,
            "shortname": Model.shortname,
            "cathedral_path": Model.cathedral_path
        };
    };

    Model.getCathedralModel = function () {
        return Model.cathedral_path ? findModelByPath(CathedralModel, Model.cathedral_path) : null;
    };

    Model.getPrettyModelName = function () {
        return 'Предмет';
    };

    Model.getPrettyItemName = function () {
        return Model.shortname;
    };

    Model.getViewUrl = function () {
        return `#!/subjects/${Model.id}`;
    };
}

//Статичні методи та властивості
SubjectModel.COLLECTION_NAME = "Subject";

SubjectModel.STORAGE = [];

(function (){
    let BaseModel = SubjectModel;
    let globalChangeTriggerId = "globalChangeTriggerSubject";
    pullCollection(BaseModel, globalChangeTriggerId);
})();
