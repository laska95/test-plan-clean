'use strict';

function LessonPlanModel() {

    const Model = this;

    Model._snapshot = null;

    Model.id = null;

    Model.weekparity = 1;
    Model.weekday = 1;
    Model.number = 1;
    Model.lessontype = 1;

    Model.subject_path = null;
    Model.auditory_path = null;
    Model.lecturer_path = null;
    Model.group_pathes = [];

    Model.comment = "";

    Model.restoreBySnapshot = function () {

        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.weekparity = data.weekparity;
        Model.weekday = data.weekday;
        Model.number = data.number;
        Model.lessontype = data.lessontype;

        Model.subject_path = data.subject_path;
        Model.auditory_path = data.auditory_path;
        Model.lecturer_path = data.lecturer_path;
        Model.group_pathes = data.group_pathes;
        Model.comment = data.comment;

    };

    Model.getPath = function () {
        return `${LessonPlanModel.COLLECTION_NAME}/${Model.id}`;
    };

    Model.getFullData = function () {
        return {
            "weekparity": Model.weekparity,
            "weekday": Model.weekday,
            "lessontype": Model.lessontype,
            "number": Model.number,

            "subject_path": Model.subject_path,
            "auditory_path": Model.auditory_path,
            "lecturer_path": Model.lecturer_path,
            "group_pathes": Model.group_pathes,
            "comment": Model.comment
        };
    };

    Model.getWeekparity = function(){
        return LessonPlanModel.weekparityList.find(item => {
           return (item.value == Model.weekparity);
        });
    };

    Model.getWeekday = function(){
        return LessonPlanModel.weekdayList.find(item => {
            return (item.value == Model.weekday);
        });
    };

    Model.getNumber = function(){
        return LessonPlanModel.numberList.find(item => {
            return (item.value == Model.number);
        });
    };

    Model.getLessonType = function(){
        return LessonPlanModel.lessonTypeList.find(item => {
            return (item.value == Model.lessontype);
        });
    };

    Model.getCathedralModel = function () {
        return Model.cathedral_path ? findModelByPath(CathedralModel, Model.cathedral_path) : null;
    };

    Model.getSubjectModel = function(){
        return Model.subject_path? findModelByPath(SubjectModel, Model.subject_path) : null;
    };

    Model.getAuditoryModel = function(){
        return Model.auditory_path? findModelByPath(AuditoryModel, Model.auditory_path) : null;
    };

    Model.getLecturerModel = function(){
        return Model.lecturer_path? findModelByPath(LecturerModel, Model.lecturer_path) : null;
    };

    Model.getPrettyModelName = function () {
        return 'Заняття';
    };

    Model.getPrettyItemName = function () {
        return '';
    };

    Model.getViewUrl = function () {
        return `#!/lessons/${Model.id}`;
    };

}

//Статичні методи та властивості
LessonPlanModel.COLLECTION_NAME = "LessonPlan";

//Оголосивши списки констант тут, ми тепер можемо до них звертатися з будь якого місця в додатку
//якщо б вони залишилися в контроллері, нам би прийшлося дубювати код
LessonPlanModel.weekparityList = [
    {"value": 1, "label": "Непарний"},
    {"value": 2, "label": "Парний"},
];

LessonPlanModel.weekdayList = [
    {"value": 1, "label": "Понеділок"},
    {"value": 2, "label": "Вівторок"},
    {"value": 3, "label": "Середа"},
    {"value": 4, "label": "Четвер"},
    {"value": 5, "label": "П'ятниця"},
];

LessonPlanModel.numberList = [
    {"value": 1, "label": "1"},
    {"value": 2, "label": "2"},
    {"value": 3, "label": "3"},
    {"value": 4, "label": "4"},
    {"value": 5, "label": "5"},
];

LessonPlanModel.lessonTypeList = [
    {"value": 1, "label": "Лекція"},
    {"value": 2, "label": "Практична"},
];

LessonPlanModel.STORAGE = [];

(function (){
    let BaseModel = LessonPlanModel;
    let globalChangeTriggerId = "globalChangeTriggerLessonPlan";
    pullCollection(BaseModel, globalChangeTriggerId);
})();
