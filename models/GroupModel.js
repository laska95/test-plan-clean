'use strict';

function GroupModel() {

    const Model = this;

    Model._snapshot = null;

    Model.id = null;
    Model.name = "";
    Model.students_count = null;
    Model.cathedral_path = null;
    Model.comment = "";

    Model._errors = {};

    Model.restoreBySnapshot = function () {
        //Оновлює значення полів в моделі згідно snapshot
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.name = data.name;
        Model.students_count = data.students_count ? Number(data.students_count) : null;

        //лишаю, бо це дозволить вносити групи з інших кафедр і показувати викладачам весь їх розклад
        Model.cathedral_path = data.cathedral_path;
        Model.comment = data.comment;
    };

    Model.getPath = function () {
        return `${GroupModel.COLLECTION_NAME}/${Model.id}`;
    };

    Model.getFullData = function () {
        return {
            "name": Model.name,
            "students_count": Model.students_count,
            "cathedral_path": Model.cathedral_path,
            "comment": Model.comment
        };
    };

    Model.getCathedralModel = function () {
        return Model.cathedral_path ? findModelByPath(CathedralModel, Model.cathedral_path) : null;
    };

    Model.getSubGroups = function () {
        return filterByAttr(SubgroupModel, 'group_path', Model.getPath());
    };

    Model.getPrettyModelName = function () {
        return 'Група';
    };

    Model.getPrettyItemName = function () {
        return Model.name;
    };

    Model.getViewUrl = function () {
        return `#!/groups/${Model.id}`;
    };
}

GroupModel.COLLECTION_NAME = "Group";

GroupModel.STORAGE = [];

(function () {
    let BaseModel = GroupModel;
    let globalChangeTriggerId = "globalChangeTriggerGroup";
    pullCollection(BaseModel, globalChangeTriggerId);
})();
