'use strict';

function SubgroupModel() {

    const Model = this;

    Model._snapshot = null;

    Model.id = null;
    Model.name = "";
    Model.students_count = null;
    Model.comment = "";
    Model.group_path = "";

    Model.restoreBySnapshot = function () {
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.name = data.name;
        Model.students_count = data.students_count ? Number(data.students_count) : null;
        Model.comment = data.comment;
        Model.group_path = data.group_path;
    };

    Model.validate = function () {

        Model.filter();

        let errors = {};

        if (!Model.name) {
            errors.name = "Ім'я є обов'язковим";
        }

        if (!Model.group_path) {
            errors.group_path = "Кафедра є обов'язковою";
        }

        Model._errors = errors;
        return !Object.keys(errors).length;
    };

    Model.getFullData = function () {
        return  {
            "name": Model.name,
            "students_count": Model.students_count,
            "group_path": Model.group_path,
            "comment": Model.comment
        };
    };

    Model.getPath = function () {
        return `${SubgroupModel.COLLECTION_NAME}/${Model.id}`;
    };

    Model.getGroupModel = function () {
        return Model.group_path ? findModelByPath(GroupModel, Model.group_path) : null;
    };

    Model.getPrettyModelName = function () {
        return 'Підгрупа';
    };

    Model.getPrettyItemName = function () {
        let group = Model.getGroupModel();
        return `(${group.name}) ${Model.name}`;
    };

    Model.getViewUrl = function () {
        //підгрупа не має власної сторінки перегляду, тому повертаємо сторінку для перегляду гурпи
        let group = Model.getGroupModel();
        return `#!/groups/${group.id}`;
    };

}

SubgroupModel.COLLECTION_NAME = "Subgroup";

SubgroupModel.STORAGE = [];

(function (){
    let BaseModel = SubgroupModel;
    let globalChangeTriggerId = "globalChangeTriggerSubgroup";
    pullCollection(BaseModel, globalChangeTriggerId);
})();