'use strict';

function CathedralModel() {

    const Model = this;

    Model._snapshot = null;

    Model.id = null;
    Model.full_name = ""; //повна назва кафедри
    Model.short_name = ""; //абрівіатура

    Model.restoreBySnapshot = function(){
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.full_name = data.full_name;
        Model.short_name = data.short_name;
    };

    Model.getPath = function () {
        return `${CathedralModel.COLLECTION_NAME}/${Model.id}`;
    };

    Model.getFullData = function () {
        return {
            "full_name": Model.full_name,
            "short_name": Model.short_name
        };
    };

    Model.getPrettyModelName = function () {
        return 'Кафедра';
    };

    Model.getPrettyItemName = function () {
        return Model.short_name;
    };

    Model.getViewUrl = function () {
        return `#!/cathedrals/${Model.id}`;
    };

}

CathedralModel.COLLECTION_NAME = "Cathedral";

CathedralModel.STORAGE = [];

(function () {
    let BaseModel = CathedralModel;
    let globalChangeTriggerId = "globalChangeTriggerCathedral";

    pullCollection(BaseModel, globalChangeTriggerId);

})();
