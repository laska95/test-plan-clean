'use strict';

function LecturerModel() {

    const Model = this;
    const COLLECTION_NAME = LecturerModel.COLLECTION_NAME;

    Model._snapshot = null;

    Model.id = null;
    Model.surname = "";
    Model.name = "";
    Model.middlename = "";
    Model.cathedral_path = null;
    Model.degree = "";

    Model.restoreBySnapshot = function () {
        //Оновлює значення полів в моделі згідно snapshot
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.surname = data.surname;
        Model.name = data.name;
        Model.middlename = data.middlename;
        Model.cathedral_path = data.cathedral_path;
        Model.degree = data.degree;
    };

    Model.getPath = function () {
        return `${COLLECTION_NAME}/${Model.id}`;
    };

    Model.getFullData = function () {
        return {
            "surname": Model.surname,
            "name": Model.name,
            "middlename": Model.middlename,
            "degree": Model.degree,
            "cathedral_path": Model.cathedral_path
        };
    };

    Model.getFullName = function(){
        return `${Model.surname} ${Model.name} ${Model.middlename}`;
    };

    Model.getCathedralModel = function () {
        return Model.cathedral_path ? findModelByPath(CathedralModel, Model.cathedral_path) : null;
    };

    Model.getPrettyModelName = function () {
        return 'Викладач';
    };

    Model.getPrettyItemName = function () {
        return Model.getFullName();
    };

    Model.getViewUrl = function () {
        return `#!/lecturers/${Model.id}`;
    };
}

//Статичні методи та властивості
LecturerModel.COLLECTION_NAME = "Lecturer";

//скачуємо всю базу декількома запитами і опісля працюємо тільки з цими даними
LecturerModel.STORAGE = [];

(function (){
    let BaseModel = LecturerModel;
    let globalChangeTriggerId = "globalChangeTriggerLecturer";
    pullCollection(BaseModel, globalChangeTriggerId);
})();


