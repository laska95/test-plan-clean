'use strict';

function AuditoryModel() {

    const Model = this;

    Model._snapshot = null; //дані отримані з БД

    //атрибути моделі
    Model.id = null;
    Model.fullname = "";
    Model.size = "";

    //абсолютний шлях до кафедри, якій належить ця аудиторія. Щоб отримати дані з БД через path
    //можна використати функцію DB.ref(Model.cathedral_path).get((snapshot) => {.....})
    //(точно синтаксис не пам'ятаю, але якось так)
    Model.cathedral_path = null;

    //обов'язкова функція у всіх моделях. Описує, як дані з БД перевести в зручний для js формат,
    //тобто як заповнити поля моделі
    Model.restoreBySnapshot = function () {
        Model.id = Model._snapshot.id;

        let data = Model._snapshot.data();
        Model.fullname = data.fullname;
        Model.size = data.size;
        Model.cathedral_path = data.cathedral_path;
    };

    //Повертає абсолютний шлях до поточної моделі
    Model.getPath = function () {
        return `${AuditoryModel.COLLECTION_NAME}/${Model.id}`;
    };

    //обов'язкова функція у всіх моделях. Протилежна до restoreBySnapshot
    //описує як дані з моделі перевести в формат для запису в БД
    Model.getFullData = function () {
        return {
            "fullname": Model.fullname,
            "size": Model.size,
            "cathedral_path": Model.cathedral_path
        };
    };

    //Повертає екземпляр моделі Кафедра, якій належить ця аудиторія
    //Таких get методів в моделях багато. Вони дозволяють отримувати зв'язані дані,
    //і без додаткових звернень до БД
    Model.getCathedralModel = function () {
        //findModelByPath - одна з функцій в lib/customLFunctions.js
        return Model.cathedral_path ? findModelByPath(CathedralModel, Model.cathedral_path) : null;
    };

    //Наступні три методи також обов'язкові для всіх моделей. Написані щоб швидко побудавати таблиця
    //при видаленні якогось з елементів, і вивести всі його зв'язки
    Model.getPrettyModelName = function () {
        return 'Аудиторії';
    };

    Model.getPrettyItemName = function () {
        return Model.fullname;
    };

    Model.getViewUrl = function () {
        //шлях до сторінки з переглядом детальної інформації про поточну Аудиторію
      return `#!/auditories/${Model.id}`;
    };

    //тобто кожна модель має свої унікальні атрибути, але всі вони реалізують спільний інтерфейс,
    //що складається з методів:
    // * restoreBySnapshot
    // * getPath
    // * getFullData
    // * getPrettyModelName
    // * getPrettyItemName
    // * getViewUrl
}

//Статичні методи та властивості. Доступні без створення екземпляру моделі.
//Екземпяр моделі створюється через new ... (ex: let model = new AuditoryModel())
AuditoryModel.COLLECTION_NAME = "Auditory";

AuditoryModel.STORAGE = []; //копія БД, але тут уже зберігаються екземпляри Аудиторій, а не дані з БД в чистоу вигляді

(function () {
    let BaseModel = AuditoryModel;
    let globalChangeTriggerId = "globalChangeTriggerAuditory";
    //одна з функцій в lib/customLFunctions.js
    pullCollection(BaseModel, globalChangeTriggerId);
})();

