'use strict'; //ось це загугли.


/**
 * Запускає синхронізацію даних в реальному часі між FB і нашим додатком.
 * BaseModel - справжня назва моделі
 * globalChangeTriggerId - id одного з приххованих полів в index.html
 */
function pullCollection(BaseModel, globalChangeTriggerId) {
    DB.collection(BaseModel.COLLECTION_NAME).onSnapshot(snapshot => {
        let changes = snapshot.docChanges();
        changes.forEach(change => {
            if (change.type == "added") {
                let modelItem = constructBySnapshot(BaseModel, change.doc);
                BaseModel.STORAGE.push(modelItem);
            } else if (change.type == "removed") {
                let index = BaseModel.STORAGE.findIndex(item => (item.id == change.doc.id));
                BaseModel.STORAGE.splice(index, 1);
            } else if (change.type == "modified") {
                let index = BaseModel.STORAGE.findIndex(item => (item.id == change.doc.id));
                BaseModel.STORAGE[index] = constructBySnapshot(BaseModel, change.doc);
            }
        });

        //Ангуляр може відловити подію change, в будь якому input.
        //При зміні і отриманні нових даних з БД, генеруємо change у відповідному input (а саме з id globalChangeTriggerId)
        //щоб AngularJs роздуплився
        let TriggerElement = document.getElementById(globalChangeTriggerId);
        TriggerElement.value = (new Date().getTime()); //дата і чвс в мілісекундах останнього оновлення даних
        TriggerElement.dispatchEvent(new Event('change'));
    });
}

/**
 * Повертає екземпляр можелі з підставленими з БД даними
 * docSnapshot - те що повертає FB
 */
function constructBySnapshot(BaseModel, docSnapshot) {
    let model = new BaseModel();
    model._snapshot = docSnapshot;
    model.restoreBySnapshot();
    return model;
}

function findModelById(BaseModel, id) {
    //В кожній моделі є свій масив STORAGE де зберігається жива копія БД
    return BaseModel.STORAGE.find(item => {
        return (item.id == id);
    });
}

function findModelByAttr(BaseModel, attr, value){
    return BaseModel.STORAGE.find(item => {
        return (item[attr] == value);
    });
}

function findModelByPath(BaseModel, value){
    //для кожної моделі абсолютний шлях генерується автоматично, а не сберігається в БД
    //тому її не вдасться знайти використовуючи функцію findModelByAttr
    return BaseModel.STORAGE.find(item => {
        return (item.getPath() == value);
    });
}

function filterByAttr(BaseModel, attr, value){
    return BaseModel.STORAGE.filter(item => {
        return (item[attr] == value);
    });
}

/**
 * Для того, щоб швидко перевірити що з чим зв'язане. Використовується при валідації перед видаленням
 * @param ModelList - список моделей в яких треба шукати використання поточної
 * @param path_attr - назва атрибуту в якому зберігається шлях поточної моделі
 * @param thisModelPathValue - абсолютний шлях поточної моделі
 * */
function checkUsed(ModelList, path_attr, thisModelPathValue) {

    let usedIn = [];

    ModelList.forEach((Model) => {
        if (Model.STORAGE) {
            Model.STORAGE.forEach(item => {
                if (item[path_attr] === thisModelPathValue) {
                    usedIn.push({
                        'model_name': item.getPrettyModelName(),
                        'item_name': item.getPrettyItemName(),
                        'url': item.getViewUrl()
                    });
                }
            });
        }
    });

    return usedIn;
}