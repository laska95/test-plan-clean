'use strict';

firebase.initializeApp(GLOBAL_FIREBASE_CONFIG);
const DB = firebase.firestore();
const AUTH = firebase.auth();
