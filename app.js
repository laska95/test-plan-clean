'use strict';

const myApp = angular.module('myApp', [
    'ngRoute',
    //всі модулі що використовуються треба підключити тут
    //кожен модуль має свою унікальну url адресу
    'myApp.auth',
    'myApp.lecturers',
    'myApp.cathedrals',
    'myApp.auditories',
    'myApp.subjects',
    'myApp.lessons',
    'myApp.groups',
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/auth'});
}]);

myApp.controller('AppController', function ($scope, $rootScope, $timeout, $window) {

    //$rootScope - супер глобальна область видимості.
    //спільна для всього angularjs додатку
    $scope.global = $rootScope;

    $scope.global.globalChangeTriggerLecturer = 0;
    $scope.global.globalChangeTriggerCathedral = 0;
    $scope.global.globalChangeTriggerGroup = 0;
    $scope.global.globalChangeTriggerLessonPlan = 0;
    $scope.global.globalChangeTriggerAuditory = 0;
    $scope.global.globalChangeTriggerSubgroup = 0;
    $scope.global.globalChangeTriggerSubject = 0;

    //робимо копію мисиву з емейлами, щоб було важче сфальсифікувати дані
    $scope.adminUsers = JSON.parse(JSON.stringify(ADMIN_USERS));

    this.isAuth = false;

    //Змінна AUTH оголошена в файлі firebase/firebase.js
    AUTH.onAuthStateChanged(user => {
        this.isAuth = !!user;
        $timeout(() => $scope.$apply()); // цей рядок перебудовує відображення частини сторінки, за якувідповідає поточний контроллер
        if (!this.isAuth){
            $window.location.href = "#!/auth";
            return;
        }

        let index = $scope.adminUsers.findIndex((item) => {
            return  user.email == item;
        });

        if (index < 0){
            //в систему намагається ввійти НЕ адміністратор
            //блокуємо такий вхід
            AUTH.signOut();
            $window.location.href = "#!/auth";
            return;
        }
    });

});

